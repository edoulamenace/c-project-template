/**
 * @file
 * Lib Goodbye
 **/
#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include <goodbye.h>

/**
 * Say Goodbye, DO NOT have fun.
 * 
 * @param str your sad message
 * @returns always 1 
 */
int goodbye(char *str){
    printf("%s", str);
    return 1;
}