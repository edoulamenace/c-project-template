#include <hello.h>
#include <stdio.h>

int __wrap_printf(const char *__restrict__ __format, ...) {
    __real_printf("Don't care what you wanted to say: printf was mocked!\n");
    return 0;
}


int main() {
    hello("Oulala!\n");
    return 0;
}