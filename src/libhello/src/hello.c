/**
 * @file
 * Lib hello
 **/
#include <stdio.h>
#include <stdlib.h>
#include <project.h>
#include <hello.h>

/**
 * Say hello, have fun!
 * 
 * @param str your awesome message
 * @returns always 0 
 */
int hello(char *str){
    printf("%s", str);
    return 0;
}