FROM python

RUN apt-get update -y && apt-get install build-essential cmake doxygen -y
RUN pip install sphinx breathe sphinx_rtd_theme

COPY . /code
RUN mkdir -p /code/build
WORKDIR /code/build


CMD cmake .. && cmake --build . && ctest
