******************************
C Project Template
******************************

Setup Docker
#############################
First, login into the gitlab registry:
::

    docker login registry.gitlab.com

Then, you can build your custom Docker image like so:

Build the image:
::

    docker build -t registry.gitlab.com/edoulamenace/c-project-template .

Push to Docker registry:
::

    docker push registry.gitlab.com/edoulamenace/c-project-template


**Don't forget to change the name of the image to your own directory!**

Finally, edit .gitlab-ci.yml to change the ``image`` fields to the name of your own image.


Supported Platforms
########################
- Linux
- Windows
- MacOS  


Build Steps
#########################
.. code-block:: sh
    :linenos:

    mkdir build && cd build
    cmake ..
    cmake --build .

More build options are documented `here <./docs/build.rst>`.

Testing
##########################
.. code-block:: sh
    :linenos:

    mkdir build && cd build
    cmake -DBUILD_TESTS=ON ..
    cmake --build .
    ctest

More test options are documented `here <./docs/build.rst>`.

Build with Docker
##############################
.. code-block:: sh
    :linenos:

    docker build -t c-project-template .
    docker run -i -t --volume=`realpath ./build`:/code/build c-project-template

You may need to run these commands as superuser, depending on your docker configuration.

Restructure Text (.rst)
#############################
The markup language of the documentation is ``ReStructureText``. If you're looking for a cheatsheet of the language, look at https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html

Table of content
#############################
.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Guides
   
   docs/*

.. toctree::
   :maxdepth: 2
   :glob:
   :caption: Documentation

   .src/*/README

