function(register_test)
    ##
    # Required arguments:
    #   NAMESPACE -> Namespace for the test
    #   NAME      -> Name of the test
    #   FILES     -> List of the files to be compiled to executable
    #   WRAPPED   -> List of the function to be wrapped during compilation
    #                 (used to mock functions)
    #   LINKED    -> List of the libraries to link to the executable
    ##

    set(options)
    set(oneValueArgs NAME NAMESPACE)
    set(multiValueArgs FILES WRAPPED LINKED)
    cmake_parse_arguments(R_TEST "${options}" "${oneValueArgs}"
                          "${multiValueArgs}" ${ARGN} )
    
    # Set the real name
    set(R_TEST_REALNAME "Test-${R_TEST_NAMESPACE}-${R_TEST_NAME}")

    # Compute the -Wl,-wrap=<func> flags list
    set(R_TEST_WRAPPED_FLAGS "")
    foreach(FUNC_TO_NAME ${R_TEST_WRAPPED})
        LIST(APPEND R_TEST_WRAPPED_FLAGS "-Wl,-wrap=${FUNC_TO_NAME}")
    endforeach()



    # Register the test
    add_executable(${R_TEST_REALNAME} ${R_TEST_FILES})
    target_include_directories(${R_TEST_REALNAME} PUBLIC
                                "${PROJECT_BINARY_DIR}"
                                "${PROJECT_SOURCE_DIR}/include"
                                )
    target_link_libraries(${R_TEST_REALNAME} PUBLIC ${R_TEST_WRAPPED_FLAGS} ${R_TEST_LINKED})
    add_test(NAME ${R_TEST_REALNAME} COMMAND ${R_TEST_REALNAME})
endfunction(register_test)