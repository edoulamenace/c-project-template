macro(register_module)
    ##
    # Required arguments:
    #   NAME  -> Name of the module
    #   PATH  -> Where the module is located.
    #
    # Sets these variables globally:
    #   MODULE_<name given to the module with 'NAME' ARG>: Name of the build target
    ##

    set(options)
    set(oneValueArgs NAME PATH)
    set(multiValueArgs)
    cmake_parse_arguments(R_MODULE "${options}" "${oneValueArgs}"
                          "${multiValueArgs}" ${ARGN} )

    option(BUILD_MODULE_${R_MODULE_NAME} "Build the ${R_MODULE_NAME} library." ON)
    if(BUILD_ALL OR BUILD_MODULE_${R_MODULE_NAME})
        add_subdirectory(${R_MODULE_PATH})
    endif()
endmacro(register_module)