Testing
=============================

Run the test suite
------------------------
The following command will run the test suite:
::

    mkdir build && cd build
    cmake ..
    cmake --build .
    ctest

By default, the output of the tests is really quiet. It will only tell whether or not the tests passed. To get more infos, use the ```--verbose``` flag with ```ctest```:
::

    # ...
    ctest --verbose


Run the test suite with Docker
--------------------------------
Run the test suite inside a Docker container using the following commands:
::

    docker build -t c-project-template .
    docker run -i -t --volume=`realpath ./build`:/code/build c-project-template

You may need to run the command as superuser 