Build Steps
========================

Easy Build
-------------------
Simplest way to build the project:
::

    mkdir build && cd build
    cmake ..
    cmake --build .


Override Options
--------------------
To get a list of the available options **(run in the build directory)**:
::

    cmake -L ..

You can override an option with the ``-D`` flag like so:
::

    cmake -DBUILD_ALL=OFF ..
    cmake --build .

where ``BUILD_ALL`` is the option to override, setting its value to ``OFF``


Disable tests build
----------------------
If you don't want to build the tests:
::

    cmake -DBUILD_TESTING=OFF ..
    cmake --build .


Remove and add modules in the build
-----------------------------------------
You can remove modules from the build. As long as the ``BUILD_ALL`` option is ``ON``, everything will be built. To remove modules from the build:
1. Set ``BUILD_ALL=OFF``
2. Set ``BUILD_MODULES_<name of the module you wish to disable>=OFF``

Example:
::

    cmake -DBUILD_ALL=OFF -DBUILD_MODULES_HELLO=OFF ..
    cmake --build .